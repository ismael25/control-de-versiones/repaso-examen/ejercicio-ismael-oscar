# Skillset using HTML5 progress bars with CSS3 animations

A Pen created on CodePen.io. Original URL: [https://codepen.io/pankajparashar/pen/qDKGo](https://codepen.io/pankajparashar/pen/qDKGo).

In this demo, we will use HTML5 progress element to display skillset. We will try and make this as cross-browser as possible with decent fallback tachniques for browsers that do not support them.

This is actually a walkthrough of my article on CSS-Tricks : http://css-tricks.com/html5-progress-element/ for the same topic.

Code walkthrough - http://thecodeplayer.com/walkthrough/361f10bb1e1f5116fad2f6f0825854dc

Hope, you will enjoy it!


